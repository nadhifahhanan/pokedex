/* eslint-disable max-len */

import React, { useState, useEffect } from 'react'
import { Container } from 'semantic-ui-react';
import { Segment } from 'semantic-ui-react';
import Items from "../Item/Items.js";

export default function Containers() {

    const [data, setData] = useState({
        pokemon: [],
    });

    useEffect(() => {
        // Update the document title using the browser API
        const apiUrl = 'https://pokeapi.co/api/v2/pokemon';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setData({
                pokemon: data['results']
            }));
    });

    return (

        <Container className="base-container">

            <Segment className="segment">
                {data.pokemon.map((Item) => {
                    return <Items
                        pokemon={Item} />
                })}
            </Segment>

        </Container>


    );
}
