import React, { useState, useEffect } from 'react'
import { Button, Grid, Image, Modal, Header } from 'semantic-ui-react'

export default function Items(props) {
    const [open, setOpen] = React.useState(false);
    const [data, setData] = useState({
        sprites: [],
        id: [],
        base_exp: [],
        weight: [],
        height: [],
        stats: [],
        types: [],
        abilities: [],
        moves: []

    });

    useEffect(() => {
        // Update the document title using the browser API
        const apiUrl = props.pokemon.url;
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setData({
                sprites: data['sprites'],
                id: data['id'],
                base_exp: data['base_experience'],
                weight: data['weight'],
                height: data['height'],
                stats: data['stats'],
                types: data['types'],
                abilities: data['abilities'],
                moves: data['moves']

            }));
    });

    return (
        <Modal
            open={open}
            onClose={() => setOpen(false)}
            onOpen={() => setOpen(true)}
            trigger={<Button className="pokemon-list">{props.pokemon.name}</Button>}
        >
            <Modal.Header>
                <div className="header-content">
                    <Image className="pokemon-image" src={data.sprites.front_default} size='small' />
                    <div className="display-grid">
                        <span className="font-reg">#{data.id}</span>
                        <span className="font-title">{props.pokemon.name}</span>
                        <span className="font-reg">Base exp <span style={{ fontWeight: "lighter", marginLeft: "4px" }}>{data.base_exp}</span></span>
                        <span className="font-reg">Weight <span style={{ fontWeight: "lighter", marginLeft: "4px" }}>{data.weight}</span></span>
                        <span className="font-reg">Height <span style={{ fontWeight: "lighter", marginLeft: "4px" }}>{data.height}</span></span>
                    </div>
                </div>

                <div>
                    <Modal.Actions>
                        <Button onClick={() => setOpen(false)} size="mini" className="button-close">
                            CLOSE
                        </Button>
                    </Modal.Actions>
                </div>

            </Modal.Header>
            <Modal.Content image scrolling>
                <Grid columns={2} divided='vertically'>
                    <Grid.Row id="row-width">
                        <Grid.Column width={8}>
                            <div className="display-grid">
                                <span className="font-sub-title">Stats</span>
                                {data.stats.map((Item) => {
                                    return <span className="font-reg">{Item.stat.name} <span style={{ fontWeight: "lighter", marginLeft: "4px" }}>{Item.base_stat}</span></span>
                                })}
                                
                            </div>
                            <div className="display-grid">
                                <span className="font-sub-title">Types</span>
                                {data.types.map((Item) => {
                                    return <span className="font-reg">{Item.type.name}</span>
                                })}
                                
                            </div>
                            <div className="display-grid">
                                <span className="font-sub-title">Abilities</span>
                                {data.abilities.map((Item) => {
                                    return <span className="font-reg">{Item.ability.name}</span>
                                })}
                            </div>
                        </Grid.Column>
                        <Grid.Column width={8}>
                            <div className="display-grid">
                                <span className="font-sub-title">Moves</span>
                                {data.moves.map((Item) => {
                                    return <span className="font-reg">{Item.move.name}</span>
                                })}
                            </div>
                        </Grid.Column>
                    </Grid.Row>

                </Grid>

            </Modal.Content>

        </Modal >

    );
}